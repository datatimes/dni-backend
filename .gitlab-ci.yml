image: docker:latest

variables:
  CONTAINER_NGINX_IMAGE: $CI_REGISTRY_IMAGE:nginx-$CI_PIPELINE_ID
  CONTAINER_NGINX_RELEASE_IMAGE: $GCP_ECR_URI:nginx-latest #release the image to external container registry
  CONTAINER_PHPFPM_IMAGE: $CI_REGISTRY_IMAGE:phpfpm-$CI_PIPELINE_ID
  CONTAINER_PHPFPM_RELEASE_IMAGE: $GCP_ECR_URI:phpfpm-latest  #release the image to external container registry
  COMPOSER_CACHE_DIR: /cache
  DOCKER_DRIVER: overlay

stages:
- meld
- composer
- build
- test
- release
- deploy

services:
- docker:dind

before_script:
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com

meld:
  stage: meld 
  image: alpine 
  before_script:
  - echo "Pulling in latest front end build to the project."
  script: 
  - wget "https://gitlab.com/api/v4/projects/$FRONTEND_PROJECT_ID/jobs/artifacts/master/download?job=build&private_token=$FRONTEND_TOKEN" -O frontend-build.zip
  artifacts:
    paths:
      - frontend-build.zip

composer:
  stage: composer
  before_script:
  - echo "Building PHP dependencies"
  image: composer:1.10
  script:
  - composer install
  dependencies:
  - meld
  artifacts:
    paths:
    - vendor
    - bootstrap/cache
    - bootstrap/autoload.php
    - composer.lock
  
build:
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
  # First we build using an image we will have to download anyway
  - unzip frontend-build.zip && mv dist/index.html resources/views/index.blade.php &&  cp -R dist/* public
  - chown -R 33 storage/logs bootstrap/cache
  - docker build -f infrastructure/containers/nginx/Dockerfile -t $CONTAINER_NGINX_IMAGE .
  - docker build -f infrastructure/containers/phpfpm/Dockerfile -t $CONTAINER_PHPFPM_IMAGE .
  - docker push $CONTAINER_NGINX_IMAGE
  - docker push $CONTAINER_PHPFPM_IMAGE
  dependencies:
  - composer
  - meld

test:
  stage: test
  script:
  - docker run -t $CONTAINER_PHPFPM_IMAGE sh -c "curl -L -o phpunit https://phar.phpunit.de/phpunit-7.2.phar && chmod +x phpunit && ./phpunit"
  #- docker run -t $CONTAINER_PHPFPM_IMAGE sh -c "curl -L -o behat https://github.com/Behat/Behat/releases/download/v3.3.0/behat.phar && chmod +x behat && ./behat"
  #- docker run -t $CONTAINER_PHPFPM_IMAGE sh -c "curl -L -o phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar && chmod +x phpcs && ./phpcs"
  #- docker run --entrypoint vendor/behat/behat/bin/behat $CONTAINER_PHPFPM_IMAGE

release:
  stage: release
  script:
  # NGINX
  - docker pull $CONTAINER_NGINX_IMAGE
  - docker tag $CONTAINER_NGINX_IMAGE $CONTAINER_NGINX_RELEASE_IMAGE
  - docker tag $CONTAINER_NGINX_RELEASE_IMAGE $GCP_ECR_URI:nginx-$CI_PIPELINE_ID
  # PHPFPM
  - docker pull $CONTAINER_PHPFPM_IMAGE
  - docker tag $CONTAINER_PHPFPM_IMAGE $CONTAINER_PHPFPM_RELEASE_IMAGE
  - docker tag $CONTAINER_PHPFPM_RELEASE_IMAGE $GCP_ECR_URI:phpfpm-$CI_PIPELINE_ID
  # Login to GCP container Registry
  - docker login -u _json_key -p "$(echo $GCP_KEY_JSON)" $GCP_ECR_URI
  # Push release versions to repository.
  - docker push $CONTAINER_NGINX_RELEASE_IMAGE
  - docker push $GCP_ECR_URI:nginx-$CI_PIPELINE_ID
  - docker push $CONTAINER_PHPFPM_RELEASE_IMAGE
  - docker push $GCP_ECR_URI:phpfpm-$CI_PIPELINE_ID
  only:
  - master
  - development
  dependencies: []

# WARNING: Prefer using --context=$CI_PROJECT_ID else concurrent builds may fail.
deploy_dev:
  image: google/cloud-sdk:300.0.0
  before_script:
  # In gitlab operation -> kubernetes needs to be setup for this to work so you get the KUBE variables
  - kubectl config set-cluster "$CI_PROJECT_ID" --server="$KUBE_URL" --certificate-authority="$KUBE_CA_PEM_FILE"
  - kubectl config set-credentials "$CI_PROJECT_ID" --token="$KUBE_TOKEN"
  - kubectl config set-context "$CI_PROJECT_ID" --cluster="$CI_PROJECT_ID" --user="$CI_PROJECT_ID" --namespace="$KUBE_NAMESPACE"
  - kubectl config use-context "$CI_PROJECT_ID"
  stage: deploy
  script:
  #- >-
  #  kubectl patch cronjob.v1beta1.batch $CI_ENVIRONMENT_NAME-laravel-phpfpm -p "{ \"spec\": { \"jobTemplate\": { \"spec\": { \"template\": { \"spec\": { \"containers\": [{ \"name\": \"laravel-phpfpm-scheduler\", \"image\": \"$GCP_ECR_URI:phpfpm-$CI_PIPELINE_ID\"}] } }} } }}"
  #- kubectl set image deployment/$CI_ENVIRONMENT_NAME-laravel-phpfpm-worker laravel-phpfpm-worker=$GCP_ECR_URI:phpfpm-$CI_PIPELINE_ID
  #- kubectl set image deployment/$CI_ENVIRONMENT_NAME-laravel-phpfpm-internal laravel-phpfpm-internal=$GCP_ECR_URI:phpfpm-$CI_PIPELINE_ID
  - kubectl get pods
  - kubectl set image deployment/$CI_ENVIRONMENT_NAME-laravel-nginx nginx=$GCP_ECR_URI:nginx-$CI_PIPELINE_ID
  - kubectl set image deployment/$CI_ENVIRONMENT_NAME-laravel-phpfpm laravel-phpfpm=$GCP_ECR_URI:phpfpm-$CI_PIPELINE_ID
  environment:
    name: dni-buckram-dev
    url: $CI_ENVIRONMENT_URL
  only:
  - development
  - master
  dependencies: []
