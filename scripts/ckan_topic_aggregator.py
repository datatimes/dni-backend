import time
import logging
import urllib
import datetime
import requests
import sys
import json

CKAN_HOST = 'https://ckan.ev.openindustry.in/api/3/action/package_show?id='
LINTOL_HOST = 'https://ltl.ev.openindustry.in'

ELASTIC_SEARCH_API = 'https://es.thedatatimes.com/ckan/_doc'
ELASTIC_SEARCH_API_LINTOL_ID = 'https://es.thedatatimes.com/ckan/_search?q=lintol_package_id:'

# ELASTIC_SEARCH_API = 'http://localhost:9200/ckan/_doc'
# ELASTIC_SEARCH_API_LINTOL_ID = 'http://localhost:9200/ckan/_search?q=lintol_package_id:'

#REPORT_API = 'https://ltl.ev.openindustry.in/api/reports/all'
# REPORT_API ='https://ltl.ev.openindustry.in/apic/v1.0/reports?since=2019-07-06+12%3A30%3A00Z'
REPORT_API ='https://ltl.ev.openindustry.in/apic/v1.0/reports?count=400'
# REPORT_API ='https://ltl.ev.openindustry.in/apic/v1.0/reports?since='

DATA_RESOURCE_API = 'https://ltl.ev.openindustry.in/apic/v1.0/dataResources?include=package&ids='
TIME_DELAY = 10
dataResourcesTopics = {}
dataResourcesIdsLintolPackageIds = {}

def get_reports(url, since):
    connected = False
    while not connected:
        try:
            results = requests.get(url);
            connected = True
        except requests.exceptions.RequestException as e:
            logging.error('Could not connect...')
            time.sleep(3)
    reports = results.json()
    get_data_resources_ids(reports)
    return reports

def get_data_resources_ids(reports):
    print('Number of reports:')
    print(len(reports['data']))
    for report in reports['data']:
       if report['attributes']['dataResourceId']:
           dataResourceId = report['attributes']['dataResourceId']
           topics = aggregate_topics(report['attributes'])
           if len(topics) > 0:
               dataResourcesTopics[dataResourceId] = topics
    print('Number of data resources with topics:')
    print(len(dataResourcesTopics))

# json.data[822].attributes.content.tables[0].informations[0]["error-data"] = [];
# json.data[822].attributes.content.tables[0].informations[1].processor = "datatimes/classify-category:1";

def aggregate_topics(attributes):
    allTopics = []
    if 'content' in attributes:
        if type(attributes['content']) == str:
            content = json.loads(attributes['content'])
        else:
            content = attributes['content']
        if len(content['tables'][0]['informations']) > 0:
           for information in content['tables'][0]['informations']:
               if information['code'] == 'possible-categories-all-weighted':
                   topics = extract_topics(information['error-data'])
                   allTopics = allTopics + topics
    return list(set(allTopics)) # list removes duplicates

# ["poverty:benefits", 0.24795256555080414]
def extract_topics(errorDataArray):
    topics = []
    threshold = 50.0
    for errorData in errorDataArray:
        relatedTopic = errorData[0]
        percentage = errorData[1]
        if percentage > threshold:
            topic = relatedTopic.split(':') 
            topics = topics + topic
    return topics

def get_data_resources(url):
    connected = False
    print('Requesting the following number of data resources:')
    print(len(dataResourcesTopics.keys()))
    ids = ','.join(dataResourcesTopics.keys())
    url = url + ids
    while not connected:
        try:
            results = requests.get(url);
            connected = True
        except requests.exceptions.RequestException as e:
            logging.error('Could not connect...')
            time.sleep(3)
    dataResources = results.json()
    print('Number of Data Resources returned:')
    print(len(dataResources['data']))
    return dataResources

def set_ckan_package_ids(dataResources):
    for dataResource in dataResources['data']:
        if 'package' in  dataResource['relationships']:
            dataResourcesIdsLintolPackageIds[dataResource['id']] = dataResource['relationships']['package']['data']['id']
    print('Number of Lintol Package Ids Found:')
    print(len(dataResourcesIdsLintolPackageIds))

def find_ckan_package(lintolPackageId, dataResources):
    for dataResourceIncluded in dataResources['included']:
        if lintolPackageId == dataResourceIncluded['id']:
            return dataResourceIncluded['attributes']['metadata']
    return ''

def meld_data(dataResources):
    index = 0
    for dataResource in dataResources['data']:
        # if index == 2:
        #     break        
        # index+=1
        dataResourceId = dataResource['id']
        if dataResourceId in dataResourcesTopics:
            topics = dataResourcesTopics[dataResourceId]
            lintolPackageId = dataResourcesIdsLintolPackageIds[dataResourceId]
            result = doesPackageExist(lintolPackageId)
            if result == False:
                ckanPackage = find_ckan_package(lintolPackageId, dataResources)
                data = {}
                source = json.loads(dataResource['attributes']['source'])
                data['lintol_package_id'] = lintolPackageId
                data['topics'] = topics
                data['source_name'] = source['harvestTitle']
                data['result'] = ckanPackage
                elasticSearchIndex = data
                sendToElasticSearch(elasticSearchIndex)
                time.sleep(5)
            else:
                print('Lintol Package ID already exists:' + lintolPackageId)

def doesPackageExist(lintolPackageId):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    response = requests.get(url = ELASTIC_SEARCH_API_LINTOL_ID + lintolPackageId, headers=headers)
    result = response.json()
    if 'hits' in result:
        if result['hits']['total'] > 0:
            return True
        else:
            print(result) 
            print('Lintol Package Id does not exist ' + lintolPackageId)
            return False
    else:
        print('No hits object available')
        return False

def buildIdQuery(ckanPackageId):
    queryObject = json.load(open('./ckanIdQuery.json'))
    queryObject['query']['nested']['query']['bool']['must'][0]['match']['result.id'] = ckanPackageId
    return queryObject

def sendToElasticSearch(data):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    response = requests.post(url = ELASTIC_SEARCH_API, data = json.dumps(data), headers = headers)
    if response.status_code == 201:
        print('Created document in index successfully')
    else:
        print('Failed to create document')
        print(response.json())
    
def run():
    hourAgo = (datetime.datetime.now() - datetime.timedelta(hours = 1)).strftime('%Y-%m-%d %H:%M:%SZ')
    print('Setting up to get reports from ' + hourAgo)
    reports = get_reports(REPORT_API, hourAgo)
    if len(reports['data']) > 0:
        dataResources = get_data_resources(DATA_RESOURCE_API)
        set_ckan_package_ids(dataResources)
        meld_data(dataResources);
        
    else:
        print('No new reports to get')

if __name__ == "__main__":
    run()
