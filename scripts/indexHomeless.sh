#!/bin/bash

#server=http://localhost:9200
server=https://es.thedatatimes.com

index=ckan

curl -X PUT "$server/_template/$index" -H "Content-Type:application/json" --data-binary "@ckanTemplate.json"
echo -e "\ntemplate index created"

curl -XDELETE "$server/$index/"
echo -e "\ndni index deleted"

if [ "$1" == "populate" ]
then
    curl -H "Content-Type: application/json" -XPOST "$server/$index/_doc/_bulk?pretty&refresh" --data-binary "@ckan.json"
    echo -e "\ndni index populated"
fi
