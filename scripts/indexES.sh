#!/bin/bash

#server=http://localhost:9200
server=https://es.thedatatimes.com
 
index=ckan

curl -XDELETE "$server/$index"
echo -e "\n $index index deleted"
curl -XDELETE "$server/queries/"
echo -e "\nqueries index deleted"

curl -X PUT "$server/_template/$index" -H "Content-Type:application/json" --data-binary "@template/ckanTemplate.json"
echo -e "\ntemplate index created"

curl -X PUT "$server/queries" -H "Content-Type:application/json" --data-binary "@queriesStructure.json"
echo -e "\nqueries index created"

if [ "$1" == "populate" ]
then
    curl -H "Content-Type: application/json" -XPOST "$server/dni/_doc/_bulk?pretty&refresh" --data-binary "@dniIndexInfo.json"
    echo -e "\ndni index populated"
fi
