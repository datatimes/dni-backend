import json

data = {1: {'title': 'Direct Provision & Related Issues', 'state': 'IE',
    'relevantOrganizations': [
        {'type': "GOV", 'description': "Reception and Integration Agency (RIA)", 'link': "http://www.ria.gov.ie/", 'contact': "http://www.ria.gov.ie/en/RIA/Pages/Contact_Us"},
        {'type': "NGO", 'description': "Nasc", 'link': "https://www.nascireland.org/", 'contact': "https://www.nascireland.org/contact"},
        {'type': "NGO", 'description': "Immigrant Council of Ireland ", 'link': "http://www.immigrantcouncil.ie/", 'contact': "http://www.immigrantcouncil.ie/contact"}
    ],
    'datasets': [
        {'description': "UNHCR's populations of concern residing in Ireland", 'id': "DFuSYW0BL3OQCrODBisv", 'organization': "UNHCR - The UN Refugee Agency"},
        {'description': "UNHCR's populations of concern originating from Ireland", 'id': "_Z2zQW0BdeI7J_hL9rAw", 'organization': "UNHCR - The UN Refugee Agency"},
        {'description': "TMQ10 - Overseas Trips to Ireland (Thousand) by Quarter and Area of Residence", 'id': "qlQbM20Bmah2eTSCYXxr", 'organization': "Central Statistics Office"}
    ],
    'articles': [
        {'date': "2019-05-03", 'description': "Calls for submissions on Direct Provision and asylum application improvements", 'outlet': "TheJournal.ie", 'author': "Cónal Thomas", 'link': "https://www.thejournal.ie/oireachtas-committee-direct-provision-justice-department-submissions-4615385-May2019/" },
        {'date': "2018-01-30", 'description': "Q&A: What is Direct Provision", 'outlet': "Irish Times", 'author': "Sorcha Pollak", 'link': "https://www.irishtimes.com/news/social-affairs/q-a-what-is-direct-provision-1.3373747" },
        {'date': "2019-09-23", 'description': "'I don't want to see these people on our streets': Charlie Flanagan admits 'crisis' in asylum seeker accommodation", 'outlet': "TheJournal.ie", 'author': "Rónán Duffy", 'link': "https://www.thejournal.ie/oireachtas-committee-direct-provision-justice-department-submissions-4615385-May2019/" },
        {'date': "2019-09-18", 'description': "Government chief whip says procedure for establishing Direct Provision centres 'unfair' on locals", 'outlet': "TheJournal.ie", 'author': " Adam Daly", 'link': "https://www.thejournal.ie/oughterard-galway-direct-provision-sean-kyne-michael-collins-4814363-Sep2019/" },
        {'date': "2019-09-14", 'description': "Protest in Oughterard over Direct Provision centre concerns", 'outlet': "TheJournal.ie", 'author': "Dominic McGrath", 'link': "https://www.thejournal.ie/direct-provision-protest-galway-oughterard-4809740-Sep2019/" }
    ],
    'description': { 
        'text': "<p>Direct Provision is a system of accommodation and living allowances for asylum-seekers in Ireland. It was established in 1999, to help manage increasing numbers of people seeking asylum, but has been criticised by a number of organisations for poor living conditions, processing times and impacts.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas rutrum, dui vitae pulvinar condimentum, leo odio dictum enim, sit amet sodales orci turpis at metus. Sed pretium feugiat libero id maximus. Nulla consectetur gravida metus, nec facilisis lacus. Vestibulum gravida tincidunt ipsum iaculis mollis. Proin porta pretium ultricies. Duis libero libero, suscipit vel est vel, vestibulum fringilla arcu. Phasellus sed aliquet nulla, eget aliquam risus. Vestibulum finibus vitae lacus vitae auctor. Sed quam turpis, sollicitudin pharetra sollicitudin sed, vulputate at turpis. Sed sit amet urna sodales, egestas nisl et, aliquam ex. Phasellus sollicitudin enim a felis pharetra sagittis. Aenean nec ligula leo. Sed sit amet justo metus. Donec eu arcu eu nisl mollis ornare eget vel urna. Sed eu ullamcorper quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>",
        'sources': [
        {'outlet': "Irish Times", "link": "https://www.irishtimes.com/news/social-affairs/q-a-what-is-direct-provision-1.3373747"},
        {'outlet': "TheJournal.ie", "link": "https://www.thejournal.ie/oireachtas-committee-direct-provision-justice-department-submissions-4615385-May2019/"}
        ]
    },
    'topics': ['inclusion', 'disabilities', 'immigration', 'ethnic group', 'refugee', 'asylum seeker', 'lgbtq'],
    'collaborativeDocuments': [
        {'title': 'Example document', 'link': 'https://docs.google.com/spreadsheets/d/1W1NUcm8c9UBnTzRYoRT3MgvThSclUGZKiwj3CA-kMeo/edit?usp=sharing'}
    ],
    },
    2: {'title': 'Cross-border Economy', 'state': 'IE',
    'relevantOrganizations': [
        {'type': "GOV", 'description': "IDA Ireland Agency", 'link': "https://www.idaireland.com/invest-in-ireland/ireland-economy", 'contact': "https://www.idaireland.com/contact-us"},
        {'type': "GOV", 'description': "Department of Finance (gov.ie)", 'link': "https://www.gov.ie/en/organisation/department-of-finance/", 'contact': "mailto:pressoffice@finance.gov.ie"},
        {'type': "NGO", 'description': "Indecon", 'link': "https://www.indecon.ie/", 'contact': "https://www.indecon.ie/contact/"}
    ],
    'datasets': [
        {'description': "Number of enterprises carrying out cross border trade by type of cross border trade and economic activity", 'id': "NULL", 'organization': "EU Open Data Portal"},
        {'description': "Cross-Border Currency Exposures", 'id': "NULL", 'organization': "International Monetary Fund"},
        {'description': "Coordinated Portfolio Investment Survey", 'id': "NULL", 'organization': "IMF Data"}
    ],
    'articles': [
        {'date': "2019-02-26", 'description': "Northern Ireland – A cross-border economy at stake", 'outlet': "Euractiv.com", 'author': "Beatriz Rios", 'link': "https://www.euractiv.com/section/economy-jobs/news/northern-ireland-a-cross-border-economy-at-stake/" },
        {'date': "2020-01-09", 'description': "The Irish Times view on multinationals in Ireland: hooked on their money", 'outlet': "Irish Times", 'author': "Martin Shanahan", 'link': "https://www.irishtimes.com/opinion/editorial/the-irish-times-view-on-multinationals-in-ireland-hooked-on-their-money-1.4135135"},
        {'date': "2020-01-30", 'description': "Bank of England says prospects for UK economy lowest since second World War", 'outlet': "Irish Times", 'author': "Boris Johnson", 'link': "https://www.irishtimes.com/business/economy/bank-of-england-says-prospects-for-uk-economy-lowest-since-second-world-war-1.4156341"},
        {'date': "2020-01-30", 'description': "Irish households 80% wealthier than at low point of recession, CSO figures show", 'outlet': "Irish Times", 'author': "Eoin Burke-Kennedy", 'link': "https://www.irishtimes.com/business/economy/irish-households-80-wealthier-than-at-low-point-of-recession-cso-figures-show-1.4156318?mode=sample&auth-failed=1&pw-origin=https%3A%2F%2Fwww.irishtimes.com%2Fbusiness%2Feconomy%2Firish-households-80-wealthier-than-at-low-point-of-recession-cso-figures-show-1.4156318"},
        {'date': "2019-09-16", 'description': "Drinks industry calls for drop in 'uncompetitive' alcohol tax as sterling value continues to fall", 'outlet': "TheJournal.ie", 'author': "Stephen McDermott", 'link': "https://www.thejournal.ie/alcohol-tax-reduction-budget-2020-4812233-Sep2019/"}
    ],
    'description': { 
        'text': "<p>An economy is an area of the production, distribution and trade, as well as consumption of goods and services by different agents. Understood in its broadest sense, 'The economy is defined as a social domain that emphasize the practices, discourses, and material expressions associated with the production, use, and management of resources'. Economic agents can be individuals, businesses, organizations, or governments. Economic transactions occur when two groups or parties agree to the value or price of the transacted good or service, commonly expressed in a certain currency.</p><p>The crossborder cooperation can boost the economic development for each participant country in the area, mostly depending on the depth of the relation between the neighbouring countries. The basis for it consists in the common interest for improvement of life standard, in ensuring a sustainable and harmonious framework and in the clearance of frontier barriers, restrictions or other factors. In view of improving of the social-economical position of the regional communities and removing of the negative effects of the border, the local communities are more and more involved in the regional collaboration. </p>",
        'sources': [
        {'outlet': "Wikipedia.org", "link": "https://en.wikipedia.org/wiki/Economy"},
        {'outlet': "Researchgate.net", "link": "https://www.researchgate.net/publication/289995861_The_Economic_Potential_of_Crossborder_Areas_Opportunities_and_Threats"}
        ]
    },
    'topics': ['economy', 'politics', 'regional policy', 'brexit', 'societies', 'trade', 'finance'],
    'collaborativeDocuments': [
        {'title': 'Example document', 'link': 'https://docs.google.com/spreadsheets/d/1W1NUcm8c9UBnTzRYoRT3MgvThSclUGZKiwj3CA-kMeo/edit?usp=sharing'}
    ],
    },
    3:  {'title': 'Violent Assaults', 'state': 'IE',
    'relevantOrganizations': [
        {'type': "NGO", 'description': "Association of Crime & Intelligence Analysts", 'link': "https://acia.org.uk", 'contact': "https://www.acia.org.uk/contact"},
        {'type': "GOV", 'description': "National Crime Agency", 'link': "https://www.nationalcrimeagency.gov.uk/", 'contact': "mailto:communication@nca.gov.uk"},
        {'type': "GOV", 'description': "NI Direct Government Services", 'link': "https://www.nidirect.gov.uk/campaigns/accessni-criminal-record-checks", 'contact': "https://www.nidirect.gov.uk/contacts/accessni"}
    ],
    'datasets': [
        {'description': "Police Recorded Crime in Northern Ireland", 'id': "NULL", 'organization': "OpenDataNI"},
        {'description': "Anti-Social Behaviour Incidents Recorded by the Police", 'id': "NULL", 'organization': "NISRA"},
        {'description': "Crimes with a domestic abuse motivation", 'id': "NULL", 'organization': "NISRA"}
    ],
    'articles': [
        {'date': "2020-02-06", 'description': "Taxi driver grabbed what he believed to be gun from man trying to rob him", 'outlet': "Irish Times", 'author': "Fiona Ferguson", 'link': "https://www.irishtimes.com/news/crime-and-law/courts/circuit-court/taxi-driver-grabbed-what-he-believed-to-be-gun-from-man-trying-to-rob-him-1.4164481" },
        {'date': "2020-02-06", 'description': "Coroner describes double killing of woman and teenage daughter as ‘barbaric'", 'outlet': "Irish Times", 'author': "Unspecified", 'link': "https://www.irishtimes.com/news/crime-and-law/courts/coroner-s-court/coroner-describes-double-killing-of-woman-and-teenage-daughter-as-barbaric-1.4164594"},
        {'date': "2020-01-30", 'description': "Violent attacks against disabled people on the rise in Northern Ireland", 'outlet': "Belfast Live", 'author': "Shauna Corr", 'link': "https://www.belfastlive.co.uk/news/belfast-news/violent-attacks-against-disabled-people-17107574"},
        {'date': "2020-01-27", 'description': "Rough justice: How police are failing survivors of sexual assault", 'outlet': "ABC News", 'author': "Inga Ting", 'link': "https://www.abc.net.au/news/2020-01-28/how-police-are-failing-survivors-of-sexual-assault/11871364?nw=0"},
        {'date': "2019-12-27", 'description': "Police issue appeal for information after serious assault", 'outlet': "Ulster Star", 'author': "Pictures Desk", 'link': "https://www.lisburntoday.co.uk/news/crime/police-issue-appeal-for-information-after-serious-assault-1-9185334"}
    ],
    'description': { 
        'text': "<p>An assault is the act of inflicting physical harm or unwanted physical contact upon a person or, in some specific legal definitions, a threat or attempt to commit such an action. It is both a crime and a tort and, therefore, may result in either criminal and/or civil liability. Generally, the common law definition is the same in criminal and tort law.</p><p>Traditionally, common law legal systems had separate definitions for assault and battery. When this distinction is observed, battery refers to the actual bodily contact, whereas assault refers to a credible threat or attempt to cause battery. Some jurisdictions combined the two offences into assault and battery, which then became widely referred to as assault. </p>",
        'sources': [
        {'outlet': "Wikipedia.org", "link": "https://en.wikipedia.org/wiki/Assault"},
        ]
    },
    'topics': ['assaults', 'kidnapping', 'robbery', 'rape', 'indecent', 'attack', 'crime'],
    'collaborativeDocuments': [
        {'title': 'Example document', 'link': 'https://docs.google.com/spreadsheets/d/1W1NUcm8c9UBnTzRYoRT3MgvThSclUGZKiwj3CA-kMeo/edit?usp=sharing'}
    ],
    },
    4:  {'title': 'Irish Language (Gaeilge)', 'state': 'IE',
    'relevantOrganizations': [
        {'type': "GOV", 'description': "Council for Gaeltacht and Gaelscoileanna Education (COGG)", 'link': "https://www.cogg.ie/en/", 'contact': "https://www.cogg.ie/en/teagmhail/"},
        {'type': "NGO", 'description': "Forbairt Feirste", 'link': "http://www.forbairtfeirste.com/", 'contact': "http://www.forbairtfeirste.com/about/contact-us/"},
        {'type': "NGO", 'description': "Gael Linn", 'link': "https://www.gael-linn.ie/", 'contact': "https://www.gael-linn.ie/en/contact-us"}
    ],
    'datasets': [
        {'description': "Irish Language (T3) by Electoral Divisions", 'id': "NULL", 'organization': "IEEEDataPort"},
        {'description': "Irish Language (T3) by Small Areas", 'id': "NULL", 'organization': "DataGovIE"},
        {'description': "Irish Sign Language Recognition", 'id': "NULL", 'organization': "Insight Centre"}
    ],
    'articles': [
        {'date': "2019-12-30", 'description': "Number of students in Irish-medium education could double under government plans", 'outlet': "Thejournal.ie", 'author': "Press Association", 'link': "https://www.thejournal.ie/irish-schools-language-joe-mchugh-education-4949993-Dec2019/" },
        {'date': "2019-11-17", 'description': "Parents threaten legal action after accusing Dundalk school of sidelining Irish language voice on board", 'outlet': "Thejournal.ie", 'author': "Dominic McGrath", 'link': "https://www.thejournal.ie/colaiste-chu-chulainn-colaiste-lu-dundalk-irish-language-4892859-Nov2019/?utm_source=story"},
        {'date': "2020-01-23", 'description': "Irish speakers fear future 'battles' over language", 'outlet': "BBC News", 'author': "Robbie Meredith", 'link': "https://www.bbc.co.uk/news/uk-northern-ireland-51215199"},
        {'date': "2020-01-10", 'description': "Stormont Irish language proposals 'fall short'", 'outlet': "Irish News", 'author': "Michael McHugh", 'link': "https://www.irishnews.com/news/northernirelandnews/2020/01/10/news/stormont-irish-language-proposals-fall-short--1811107/"},
        {'date': "2019-05-07", 'description': "This Seachtain na Gaeilge, the Irish language is free at last and it’s going global", 'outlet': "Irish Central", 'author': "Cahir O'Doherty", 'link': "https://www.irishcentral.com/news/irishvoice/irish-language-free-global"}
    ],
    'description': { 
        'text': "<p>Irish is a Celtic language (as English is a Germanic language, French a Romance language, and so on). This means that it is a member of the Celtic family of languages. Its “sister” languages are Scottish Gaelic and Manx (Isle of Man); its more distant “cousins” are Welsh, Breton and Cornish.</p><p>Irish has the earliest attested vernacular European literature outside the classical world of Greece and Rome; there is evidence for a literary tradition in Irish as early as the sixth century A.D. and evidence for literacy predates that. The medieval literary tradition has excited the imaginations of scholars all over the world, incorporating as it does some of the most extensive saga literature to be found anywhere. Despite its position on the continent’s northwestern periphery, Ireland has always had a lively engagement with the European mainland.</p>",
        'sources': [
        {'outlet': "Irishlanguage.nd.edu", "link": "https://irishlanguage.nd.edu/about/what-is-irish/"},
        ]
    },
    'topics': ['languages', 'local', 'culture', 'irish', 'celtic', 'commucation', 'education'],
    'collaborativeDocuments': [
        {'title': 'Example document', 'link': 'https://docs.google.com/spreadsheets/d/1W1NUcm8c9UBnTzRYoRT3MgvThSclUGZKiwj3CA-kMeo/edit?usp=sharing'}
    ]
    },
    5:  {'title': 'Coronavirus - COVID 19', 'state': 'IE',
    'relevantOrganizations': [
        {'type': "GOV", 'description': "NHS UK", 'link': "https://www.nhs.uk/conditions/coronavirus-covid-19/", 'contact': "https://www.nhs.uk/contact-us/"},
        {'type': "GOV", 'description': "World Health Organization", 'link': "https://www.who.int/emergencies/diseases/novel-coronavirus-2019/advice-for-public", 'contact': "https://www.who.int/about/who-we-are/contact-us"},
        {'type': "GOV", 'description': "HSC - Public Health Agency", 'link': "https://www.publichealth.hscni.net/", 'contact': "https://www.publichealth.hscni.net/node/4802"}
    ],
    'datasets': [
        {'description': "Geographic distribution of COVID-19 cases worldwide", 'id': "NULL", 'organization': "European Centre for Disease Prevention and Control"},
        {'description': "CORONA VIRUS (COVID-19) Tweets Dataset", 'id': "NULL", 'organization': "IEEEDataPort"},
        {'description': "Novel Coronavirus (COVID-19) Cases Data", 'id': "NULL", 'organization': "The Humanitarian Data Exchange"}
    ],
    'articles': [
        {'date': "2020-03-19", 'description': "Coronavirus: First death confirmed in NI", 'outlet': "BBC News", 'author': "Unknown", 'link': "https://www.bbc.co.uk/news/uk-northern-ireland-51958241" },
        {'date': "2020-03-19", 'description': "Coronavirus: NI healthcare providers ‘seriously worried’ over personal protection equipment suppl", 'outlet': "Belfast Telegraph", 'author': "Allan Preston", 'link': "https://www.belfasttelegraph.co.uk/news/health/coronavirus/coronavirus-ni-healthcare-providers-seriously-worried-over-personal-protection-equipment-supply-39057857.html"},
        {'date': "2020-03-19", 'description': "Coronavirus UK: how many confirmed cases are in your area?", 'outlet': "The Guardian", 'author': "Sean Clarke", 'link': "https://www.theguardian.com/world/2020/mar/19/coronavirus-uk-cases-how-many-in-your-area"},
        {'date': "2020-03-14", 'description': "Coronavirus: Northern Ireland schools 'will close for at least 16 weeks'", 'outlet': "The Guardian", 'author': "Michael McHugh", 'link': "https://www.theguardian.com/world/2020/mar/14/coronavirus-northern-ireland-schools-will-close-for-at-least-16-weeks"},
        {'date': "2020-03-18", 'description': "Coronavirus: What Northern Ireland businesses want now", 'outlet': "BBC News", 'author': "John Campbell", 'link': "https://www.bbc.co.uk/news/uk-northern-ireland-51942774"}
    ],
    'description': { 
        'text': "<p>COVID-19 is a new illness that can affect your lungs and airways. It's caused by a virus called coronavirus. Symptoms of coronavirus (COVID-19) are a cough, a high temperature and shortness of breath. Simple measures like washing your hands often with soap and water can help stop viruses like coronavirus (COVID-19) spreading. There's no specific treatment for coronavirus (COVID-19). Treatment aims to relieve the symptoms until you recover. It's not known exactly how coronavirus (COVID-19) spreads from person to person, but similar viruses are spread in cough droplets.</p>",
        'sources': [
        {'outlet': "nhs.uk", "link": "https://www.nhs.uk/conditions/coronavirus-covid-19/#overview"},
        ]
    },
    'topics': ['health', 'pandemic', 'virus', 'infectious diseases', 'biology', 'medical research', 'attack'],
    'collaborativeDocuments': [
        {'title': 'Example document', 'link': 'https://docs.google.com/spreadsheets/d/1W1NUcm8c9UBnTzRYoRT3MgvThSclUGZKiwj3CA-kMeo/edit?usp=sharing'}
    ]
    },
    }


json_data = json.dumps(data)
f = open("investigationPagesData.json", "a")
f.write(json_data)
f.close()
