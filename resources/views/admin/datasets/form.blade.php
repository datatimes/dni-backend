@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'link',
        'label' => 'Link',
        'maxlength' => 100
    ])

    @formField('select', [
        'name' => 'organization_id',
        'label' => 'Organization',
        'placeholder' => 'Select the organization who published this dataset',
        'options' => $organization,
    ])
@stop
