@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'maxlength' => 150
    ])

    @formField('select', [
	'name' => 'type',
	'label' => 'Type of article',
	'unpack' => true,
	'options' => [
            [
                'value' => 'newspaper',
                'label' => 'Newspaper'
            ],
            [
                'value' => 'education',
                'label' => 'Educational report'
            ],
        ]
    ])

    @formField('input', [
        'name' => 'author',
        'label' => 'Author',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'link',
        'label' => 'Link to the article',
    ])

    @formField('input', [
        'name' => 'outlet',
        'label' => 'Outlet',
    ])

    @formField('input', [
        'name' => 'published_at',
        'label' => 'Published at',
    ])
@stop
