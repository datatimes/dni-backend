@formField('input', [
    'name' => 'name',
    'label' => 'Title of the page',
    'translated' => $translateTitle ?? false,
    'required' => true,
    'onChange' => 'formatPermalink'
])

@formField('input', [
    'name' => 'descriptionAsText',
    'label' => 'Description',
    'translated' => $translateTitle ?? false,
    'required' => true,
    'onChange' => 'formatPermalink'
])


