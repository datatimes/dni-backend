@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'name',
        'label' => 'Title of the page',
        'maxlength' => 120
    ])

    @formField('wysiwyg', [
        'name' => 'descriptionAsText',
        'label' => 'Description of the theme',
        'toolbarOptions' => ['list-ordered', 'list-unordered', 'bold', 'italic', 'underline'],
        'placeholder' => 'Enter your description here...',
        'maxlength' => 2000,
        'note' => 'Hint message'
    ])

    @formField('browser', [
        'name' => 'topics',
        'label' => 'Topics',
        'min' => 1,
        'max' => '20',
        'moduleName' => 'Topics'
    ])

    @formField('input', [
        'name' => 'foi_feed_link',
        'label' => 'FOI Feed URL',
        'maxlength' => 2000 
    ])

    @formField('browser', [
	'label' => 'Articles',
	'max' => 5,
	'name' => 'articles',
	'moduleName' => 'Articles'
    ])

    @formField('browser', [
	'label' => 'Organizations',
	'max' => 5,
	'name' => 'organizations',
	'moduleName' => 'Organizations'
    ])

    @formField('browser', [
	'label' => 'Datasets',
	'max' => 5,
	'name' => 'datasets',
	'moduleName' => 'Datasets'
    ])

    @formField('files', [
    'name' => 'graphics',
    'label' => 'Interactive graphics',
    'noTranslate' => true,
    'max' => 4,
    'note' => 'Upload a Vega graphic in .json format'
    ])

@stop
