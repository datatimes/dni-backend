@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'type',
        'label' => 'Type',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'link',
        'label' => 'Link',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'contact',
        'label' => 'Contact',
        'maxlength' => 100
    ])
@stop
