<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class InvestigationPageController extends ModuleController
{

    protected $moduleName = 'investigationPages';

    protected $indexOptions = [
        'create'          => true,
        'edit'            => true,
        'publish'         => false,
        'bulkPublish'     => false,
        'feature'         => false,
        'bulkFeature'     => false,
        'restore'         => true,
        'bulkRestore'     => true,
        'delete'          => true,
        'bulkDelete'      => true,
        'reorder'         => false,
        'permalink'       => true,
        'bulkEdit'        => true,
        'editInModal'     => false,
        'forceDelete'     => true,
        'bulkForceDelete' => true,
    ];

    protected $titleColumnKey = 'name';

    protected $indexColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name',
        ],
    ];


    protected function formData($request)
    {
        $article = app(\App\Repositories\InvestigationPageRepository::class)->listAll('name');

        return ['article' => $article];

    }//end formData()


}//end class
