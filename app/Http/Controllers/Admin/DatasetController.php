<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class DatasetController extends ModuleController
{

    protected $moduleName = 'datasets';

    protected $indexOptions = [
        'create'          => true,
        'edit'            => true,
        'publish'         => false,
        'bulkPublish'     => false,
        'feature'         => false,
        'bulkFeature'     => false,
        'restore'         => true,
        'bulkRestore'     => true,
        'delete'          => true,
        'bulkDelete'      => true,
        'reorder'         => false,
        'permalink'       => true,
        'bulkEdit'        => true,
        'editInModal'     => false,
        'forceDelete'     => true,
        'bulkForceDelete' => true,
    ];

    protected $titleColumnKey = 'description';


    protected function formData($request)
    {
        $organization = app(\App\Repositories\OrganizationRepository::class)->listAll('name');

        return ['organization' => $organization];

    }//end formData()


    protected function indexData($request)
    {
        $organization = app(\App\Repositories\OrganizationRepository::class)->listAll('name');

        return ['district' => $organization];

    }//end indexData()


}//end class
