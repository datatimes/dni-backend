<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class UserController extends ModuleController
{

    protected $moduleName = 'users';
}//end class
