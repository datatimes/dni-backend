<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\TopicRepository;
use A17\Twill\Http\Controllers\Admin\ModuleController;

class TopicController extends ModuleController
{

    protected $moduleName = 'topics';

    protected $indexOptions = [
        'create'          => true,
        'edit'            => true,
        'publish'         => false,
        'bulkPublish'     => false,
        'feature'         => false,
        'bulkFeature'     => false,
        'restore'         => true,
        'bulkRestore'     => true,
        'delete'          => true,
        'bulkDelete'      => true,
        'reorder'         => false,
        'permalink'       => true,
        'bulkEdit'        => true,
        'editInModal'     => false,
        'forceDelete'     => true,
        'bulkForceDelete' => true,
    ];


    protected function formData($request)
    {
        return [
            'topics' => app()->make(TopicRepository::class)->listAll(),
        ];

    }//end formData()


}//end class
