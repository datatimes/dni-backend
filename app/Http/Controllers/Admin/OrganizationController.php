<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class OrganizationController extends ModuleController
{

    protected $moduleName = 'organizations';

    protected $indexOptions = [
        'create'          => true,
        'edit'            => true,
        'publish'         => false,
        'bulkPublish'     => false,
        'feature'         => false,
        'bulkFeature'     => false,
        'restore'         => true,
        'bulkRestore'     => true,
        'delete'          => true,
        'bulkDelete'      => true,
        'reorder'         => false,
        'permalink'       => true,
        'bulkEdit'        => true,
        'editInModal'     => false,
        'forceDelete'     => true,
        'bulkForceDelete' => true,
    ];

    protected $titleColumnKey = 'name';

}//end class
