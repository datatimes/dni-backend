<?php

namespace App\Http\Controllers;

use App\Services\TopicsService;
use Illuminate\Http\Request;

class TopicsController extends Controller
{


    public function show(Request $request, TopicsService $topicsService)
    {
        $sentence = $request->input('sentences');

        // TODO: appropriate validation
        if (! is_array($sentence) || ! count($sentence) == 1 || ! is_string($sentence[0])) {
            abort(422, 'Must send an array of one string');
        }

        $sentence = preg_replace('/[^\d\w\s]+/', '', $sentence[0]);

        $lang = app()->getLocale();

        $topics = $topicsService->getCategories($sentence, $lang);

        if (strpos($sentence, ' ') === false) {
            $locations = $topicsService->getLocations($sentence);
            $topics    = array_merge($locations, $topics);
        }

        return [$topics];

    }//end show()


}//end class
