<?php

namespace App\Http\Controllers;

use App\Services\TypesService;
use Illuminate\Http\Request;

class TypesController extends Controller
{


    public function show(Request $request, TypesService $typesService)
    {
        $reportId = $request->input('reportId');

        // TODO: appropriate validation
        \Log::info($reportId);

        $report = $typesService->getDataSummary($reportId);

        return [$report];

    }//end show()


}//end class
