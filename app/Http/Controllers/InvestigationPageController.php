<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\InvestigationPageRepository;
use Guzzle\Http\Exception\ServerErrorResponseException;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;



class InvestigationPageController extends Controller
{

    /**
     * @var investigationPageRepository
     */
    private $investigationPageRepository;


    public function __construct(InvestigationPageRepository $investigationPageRepo)
    {
        $this->investigationPageRepository = $investigationPageRepo;

    }//end __construct()


    public function getGraphics($investigationPageID, $graphicID)
    {
        $investigationPage = $this->investigationPageRepository->getById($investigationPageID);
        $fileObject        = $investigationPage->files()->find($graphicID);
        return \Storage::disk('s3')->response($fileObject->uuid);

    }//end getGraphics()


    public function getInvestigationPages()
    {
        $investigationPages = $this->investigationPageRepository->listAll();
        return $investigationPages;

    }//end getInvestigationPages()


    public function sendGraphicPictureRequest($format, $investigationPageID, $graphicID)
    {
        $investigationPage = $this->investigationPageRepository->getById($investigationPageID);
        $fileObject        = $investigationPage->files()->find($graphicID);
        $json   = \Storage::disk('local')->get($fileObject->uuid);
        $client = new Client(
            [
                'headers' => [ 'Content-Type' => 'application/json' ],
            ]
        );

        $endpoint = config('tdatim.graphics-renderer-endpoint');
        $response = $client->post(
            $endpoint.'?format='.$format,
            ['body' => $json]
        );
        \Storage::put("graphic.".$format, $response->getBody()->getContents());
        return \Storage::download("graphic.".$format);

    }//end sendGraphicPictureRequest()


    public function sendRssFeedWithCors($investigationPageID)
    {
        $investigationPage = $this->investigationPageRepository->getById($investigationPageID);
        $foiFeedLink       = $investigationPage->foi_feed_link;
        $client            = new Client();
        $response          = $client->get($foiFeedLink);
        return $response->getBody()->getContents();

    }//end sendRssFeedWithCors()


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }//end index()


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }//end create()


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }//end store()


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $investigationPage = $this->investigationPageRepository->getById($id);
        $investigationPage->load('articles');
        $investigationPage->load('organizations');
        $investigationPage->load('datasets');
        $investigationPage->load('topics');
        return [
            'investigationPage' => $investigationPage,
            'topicsList'        => $investigationPage->topics->pluck('title'),
            'graphics'          => $investigationPage->files->pluck('id'),
        ];

    }//end show()


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }//end edit()


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }//end update()


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }//end destroy()


}//end class
