<?php

namespace App\Http\Middleware;

use Closure;

class Locale
{
    public function handle($request, Closure $next)
    {
        if ($request->input('lang')) {
            app()->setLocale($request->input('lang'));
        }
        return $next($request);
    }
}
