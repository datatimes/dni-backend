<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\Behaviors\HandleBrowsers;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\InvestigationPage;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class InvestigationPageRepository extends ModuleRepository
{
    use  /*HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, */HandleFiles, UUID;
    // , HandleRevisions;
    public function __construct(InvestigationPage $model)
    {
        $this->model = $model;

    }//end __construct()


    public function afterSave($object, $fields)
    {
        $this->updateBrowser($object, $fields, 'articles');
        $this->updateBrowser($object, $fields, 'organizations');
        $this->updateBrowser($object, $fields, 'datasets');
        $this->updateBrowser($object, $fields, 'topics');

        parent::afterSave($object, $fields);

    }//end afterSave()


    public function getFormFields($object)
    {
        $fields = parent::getFormFields($object);
        $fields['browsers']['articles']      = $this->getFormFieldsForBrowser($object, 'articles');
        $fields['browsers']['organizations'] = $this->getFormFieldsForBrowser($object, 'organizations');
        $fields['browsers']['datasets']      = $this->getFormFieldsForBrowser($object, 'datasets');
        $fields['browsers']['topics']        = $this->getFormFieldsForBrowser($object, 'topics');
        return $fields;

    }//end getFormFields()


    public function listAll($column='name', $orders=[], $exceptId=null)
    {
        $query = $this->model->newQuery();

        if ($exceptId) {
            $query = $query->where($this->model->getTable().'.id', '<>', $exceptId);
        }

        if ($this->model instanceof Sortable) {
            $query = $query->ordered();
        } elseif (!empty($orders)) {
            $query = $this->order($query, $orders);
        }

        if ($this->model->isTranslatable()) {
            $query = $query->withTranslation();
        }

        return $query->get(['id', 'name']);

    }//end listAll()


}//end class
