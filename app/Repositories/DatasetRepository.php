<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Dataset;

class DatasetRepository extends ModuleRepository
{
    use HandleBlocks, HandleFiles/*, HandleRevisions*/;


    public function __construct(Dataset $model)
    {
        $this->model = $model;

    }//end __construct()


}//end class
