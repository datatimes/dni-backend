<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\User;

class UserRepository extends ModuleRepository
{
    use HandleBlocks;


    public function __construct(User $model)
    {
        $this->model = $model;

    }//end __construct()


}//end class
