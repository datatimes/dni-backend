<?php

namespace App\Services;

use Cache;
use GuzzleHttp;
use Symfony\Component\Yaml\Yaml;

class TopicsService
{

    /**
     * HTTP client for the API interactions.
     *
     * @var GuzzleHttp\Client $client HTTP client.
     */
    protected $client;

    protected $categories;

    protected $locationMaxMatches = 5;

    public function __construct(GuzzleHttp\Client $client)
    {
        $this->client = $client;
        list($this->categories, $this->categoryIndex) = $this->loadCategories();
    }

    public function loadCategories()
    {
        $cacheSeconds = config('tdatim.categories-yaml-cache-seconds', 60);
        $categories = Cache::remember(
            'tdatim.categories', $cacheSeconds, function () {
                $location = config('tdatim.categories-yaml');
                $categories = Yaml::parse(file_get_contents($location));
                return $categories;
            }
        );

        foreach ($categories as $group => $keywords) {
            $categoryIndex[$group] = [];
            foreach ($keywords as $key => $entries) {
                $categoryIndex[$group][$key] = $key;
                $categoryIndex[$group][strtolower($entries['key'])] = $key;
                if (array_key_exists('name', $entries)) {
                    $categoryIndex[$group][strtolower($entries['name'])] = $key;
                }
            }
        }

        return [$categories, $categoryIndex];
    }

    protected function filterCategories($responses, $filterSuggest=true)
    {
        $returnedResponses = [];

        foreach ($responses as $match) {
            list($category, $keyword) = $match[1];

            $category = $this->findCategory($category, $keyword);
            if ($category && (! $filterSuggest || ! array_key_exists('suggest', $category) || $category['suggest'])) {
                $returnedResponses[] = $match;
            }
        }

        return $returnedResponses;
    }

    public function findCategory($category, $keyword)
    {
        $entry = null;

        $category = strtolower($category);
        $keyword = strtolower($keyword);
        if (array_key_exists($category, $this->categoryIndex) && array_key_exists($keyword, $this->categoryIndex[$category])) {
            $entry = $this->categories[$category][$this->categoryIndex[$category][$keyword]];
        }
        return $entry;
    }

    public function getCategories($sentence, $lang, $filterSuggest=true)
    {
        $urls = config('tdatim.category-server');
        if (! array_key_exists($lang, $urls)) {
            $lang = substr($lang, 0, 2);
            $url = config('tdatim.category-server.' . $lang, config('tdatim.category-server.en'));
        } else {
            $url = $urls[$lang];
        }

        $response = $this->client->post(
            $url,
            [
                'json' => [
                    'sentences' => [$sentence]
                ]
            ]
        );

        $responses = json_decode($response->getBody())[0];

        return $this->filterCategories($responses, $filterSuggest);
    }

    public function getLocations($sentence, $countryCode='GB')
    {
        $url = config('tdatim.location-openfaas-function');

        $response = $this->client->post(
            $url,
            [
                'json' => [
                    'command' => 'QS',
                    'arguments' => [$countryCode, $this->locationMaxMatches, $sentence]
                ]
            ]
        );

        $responses = json_decode($response->getBody(), true);
        $result = [];
        foreach ($responses as $response) {
            $code = $response[1];
            $score = $response[2];
            $result[] = [$score, [$countryCode, $code['d']['name']]];
        }

        return $result;
    }
}

