<?php

namespace App\Services;

use Cache;
use GuzzleHttp;
use Symfony\Component\Yaml\Yaml;

class TypesService
{

    /**
     * HTTP client for the API interactions.
     *
     * @var GuzzleHttp\Client $client HTTP client.
     */
    protected $client;

    public function __construct(GuzzleHttp\Client $client)
    {
        $this->client = $client;
    }

    public function getDataSummary($reportId)
    {
        $url = config('tdatim.lintol-api') . '/api/v1.0/reports/' . $reportId;

        $response = $this->client->get(
            $url
        );

        $responses = json_decode($response->getBody(), true);
        $result = [];
        foreach ($responses as $response) {
            $code = $response[1];
            $score = $response[2];
            $result[] = [$score, [$countryCode, $code['d']['name']]];
        }

        return $result;
    }
}

