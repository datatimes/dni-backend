<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class InvestigationPageSlug extends Model
{

    protected $table = "investigation_page_slugs";
}//end class
