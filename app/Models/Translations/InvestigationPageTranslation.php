<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\InvestigationPage;

class InvestigationPageTranslation extends Model
{

    protected $baseModuleModel = InvestigationPage::class;
}//end class
