<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Topic;

class TopicTranslation extends Model
{

    protected $baseModuleModel = Topic::class;
}//end class
