<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Organization;

class OrganizationTranslation extends Model
{

    protected $baseModuleModel = Organization::class;
}//end class
