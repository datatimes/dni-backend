<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class Article extends Model implements Sortable
{
    use HasBlocks, HasPosition, Uuid;

    public $keyType = 'uuid';

    protected $fillable = [
        'published',
        'description',
        'position',
        'title',
        'published_at',
        'type',
        'outlet',
        'author',
        'link',
    ];

    public $mediasParams = [
        'cover' => [
            'desktop'  => [
                [
                    'name'  => 'desktop',
                    'ratio' => (16 / 9),
                ],
            ],
            'mobile'   => [
                [
                    'name'  => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name'  => 'free',
                    'ratio' => 0,
                ],
                [
                    'name'  => 'landscape',
                    'ratio' => (16 / 9),
                ],
                [
                    'name'  => 'portrait',
                    'ratio' => (3 / 5),
                ],
            ],
        ],
    ];
}//end class
