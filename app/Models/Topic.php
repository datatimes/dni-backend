<?php

namespace App\Models;

use App\Repositories\TopicRepositiory;
use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class Topic extends Model
{
    use HasRevisions, HasPosition, Uuid;

    public $keyType = 'uuid';

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
    ];

    public $mediasParams = [
        'cover' => [
            'desktop'  => [
                [
                    'name'  => 'desktop',
                    'ratio' => (16 / 9),
                ],
            ],
            'mobile'   => [
                [
                    'name'  => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name'  => 'free',
                    'ratio' => 0,
                ],
                [
                    'name'  => 'landscape',
                    'ratio' => (16 / 9),
                ],
                [
                    'name'  => 'portrait',
                    'ratio' => (3 / 5),
                ],
            ],
        ],
    ];


    public function topics()
    {
        return $this->belongsToMany('App\Models\Topic');

    }//end topics()


}//end class
