<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;


class InvestigationPage extends Model implements Sortable
{
    use HasBlocks, HasFiles, HasPosition, Uuid;

    public $keyType = 'string';
    
    public $incrementing = false;

    public $filesParams = ['graphics'];

    public $fillable = [
        'name',
        'descriptionAsText',
        'foi_feed_link',
        'topics',

    ];

    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];

    protected $casts = [
        'description'            => 'array',
        'relevant_organizations' => 'array',
        'coll_doc_link'          => 'array',
        'topics'                 => 'array',
        'articles'               => 'array',
        'datasets'               => 'array',

    ];

    public $slugAttributes = [
        'title',
    ];

    public $mediasParams = [
        'cover' => [
            'desktop'  => [
                [
                    'name'  => 'desktop',
                    'ratio' => (16 / 9),
                ],
            ],
            'mobile'   => [
                [
                    'name'  => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name'  => 'free',
                    'ratio' => 0,
                ],
                [
                    'name'  => 'landscape',
                    'ratio' => (16 / 9),
                ],
                [
                    'name'  => 'portrait',
                    'ratio' => (3 / 5),
                ],
            ],
        ],
    ];


    /**
     * The articles that belong to the Investigation page.
     */
    public function articles()
    {
        return $this->belongsToMany('App\Models\Article');

    }//end articles()


    /**
     * The datasets that belong to the Investigation page.
     */
    public function datasets()
    {
        return $this->belongsToMany('App\Models\Dataset');

    }//end datasets()


    /**
     * The organizations that belong to the Investigation page.
     */
    public function organizations()
    {
        return $this->belongsToMany('App\Models\Organization');

    }//end organizations()


    /**
     * The topics that belong to the Investigation page.
     */
    public function topics()
    {
        return $this->belongsToMany('App\Models\Topic');

    }//end topics()


    public function getDescriptionAsTextAttribute()
    {
        return $this->description['text'];

    }//end getDescriptionAsTextAttribute()


    public function setDescriptionAsTextAttribute($descriptionText)
    {
        $description         = $this->description;
        $description['text'] = $descriptionText;
        $this->description   = $description;

    }//end setDescriptionAsTextAttribute()


}//end class
