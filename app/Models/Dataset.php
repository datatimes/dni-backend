<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class Dataset extends Model implements Sortable
{
    use HasBlocks, HasFiles, HasPosition, Uuid;

    public $keyType = 'uuid';

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
        'link',
        'organization_id',
    ];


    public function organization()
    {
        return $this->belongsTo('App\Models\Organization');

    }//end organization()


    public function getTitleInBrowserAttribute()
    {
        return $this->description;

    }//end getTitleInBrowserAttribute()


}//end class
