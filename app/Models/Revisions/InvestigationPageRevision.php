<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class InvestigationPageRevision extends Revision
{

    protected $table = "investigation_page_revisions";
}//end class
