<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class TopicRevision extends Revision
{

    protected $table = "topic_revisions";
}//end class
