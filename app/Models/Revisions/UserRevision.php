<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class UserRevision extends Revision
{

    protected $table = "user_revisions";
}//end class
