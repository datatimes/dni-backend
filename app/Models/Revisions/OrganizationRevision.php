<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class OrganizationRevision extends Revision
{

    protected $table = "organization_revisions";
}//end class
