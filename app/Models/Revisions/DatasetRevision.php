<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class DatasetRevision extends Revision
{

    protected $table = "dataset_revisions";
}//end class
