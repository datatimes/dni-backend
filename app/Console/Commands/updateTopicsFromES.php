<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ServerErrorResponseException;
use GuzzleHttp\RequestOptions;

class updateTopicsFromES extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:topics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs topics to db in from Elastic Search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $url    = config('tdatim.es-proxy').'/core/_doc/_search';

        $response   = $client->request(
            'POST',
            $url,
            [
                'json' => json_decode('{"aggs":{"unique_field":{"terms":{"field":"topics","size":10000}}}}'),
            ]
        );
        $body       = $response->getBody();
        $result     = json_decode($body->getContents());
        $topicsList = $result->aggregations->unique_field->buckets;

        foreach ($topicsList as $topic) {
            \App\Models\Topic::updateOrCreate(
                ['title' => $topic->key]
            );
        }
    }
}
