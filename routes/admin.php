<?php

// Register Twill routes here (eg. Route::module('posts'))
Route::module('investigationPages');
Route::module('articles');
Route::module('organizations');
Route::module('datasets');
Route::module('topics');
