<?php

use Illuminate\Http\Request;

/*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
*/

Route::middleware('auth:api')->get(
    '/user',
    function (Request $request) {
        return $request->user();
    }
);

Route::post('/topics', 'TopicsController@show');
Route::get('/types', 'TypesController@show');
Route::get('/ip/{id}', 'InvestigationPageController@show');
Route::get('/get_investigation_pages', 'InvestigationPageController@getInvestigationPages');
Route::get('/get_rss_with_cors/{pageID}', 'InvestigationPageController@sendRssFeedWithCors')->middleware('cors');
Route::get('/send_graphic_picture_request/{format}/{pageID}/{graphicID}', 'InvestigationPageController@sendGraphicPictureRequest');
