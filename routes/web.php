<?php

/*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
*/

Route::get('/ip/{id}/graphics/{graphicId}', 'InvestigationPageController@getGraphics');

$allowedRequests = [
    'POST' => [
        '/core/_count',
        '/queries/_doc/_search',
        '/core/_doc/_search',
        '/queries/_doc',
    ],
    'GET'  => ['/core/_doc/[a-zA-Z-_0-9]*'],
];

Route::any(
    '/es/{any}',
    function ($any) use ($allowedRequests) {
        $request = request();
        $esPath  = substr($request->path(), 2);
        $query   = $request->query();
        $any     = '/es/'.$any;
        unset($query[$any]);

        if (!array_key_exists($request->method(), $allowedRequests)) {
            abort(401);
        }

        $safe = false;
        foreach ($allowedRequests[$request->method()] as $route) {
            $search = '/^\/es'.str_replace('/', '\/', $route).'$/';
            if (preg_match($search, $any)) {
                $safe = true;
            }
        }

        if (!$safe) {
            abort(401);
        }

        $queryString = explode('?', $request->fullUrl());
        $client      = new GuzzleHttp\Client();
        $url         = config('tdatim.es-proxy').$esPath;
        if (!empty($query)) {
            $url    .= '?';
            $entries = [];
            foreach ($query as $key => $value) {
                if ($key != $any) {
                    $entries[] = $key.'='.$value;
                }
            }

            $url .= implode('&', $entries);
        }

        $response = $client->request(
            $request->method(),
            $url,
            [
                'json' => json_decode($request->getContent()),
            ]
        );
        $body     = $response->getBody();
        return response($body->getContents());
    }
)->where('any', '(.*)');

Route::get(
    '/{any}',
    function () {
        return view('index');
    }
)->where('any', '(.*)');
