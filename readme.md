# Data Times Backend

## Setup Locally

You will also need https://gitlab.com/datatimes/dni-frontend cloned one directory above (i.e. it should be present at `../dni-frontend`).

### Install bookcloth

Requires a Python 3 installation. From the current directory:

* Enter the following at 'dni-backend' dir:   
    `git submodule update --init`  
* Navigate to the python folder:  
    `cd infrastructure/python`  
* Run the following command in the python directory:   
    `python3 setup.py develop --prefix=$HOME/.local`  
* If you get permissions error, or bad pythonpath, add the following to .bashrc in home directory:  
    `sudo chown -R <local username here> $HOME/.local`  
    `export PYTHONPATH=$PYTHONPATH:/home/<local username here>/.local/lib/python3.7/site-packages/`  
    `export PATH=$PATH:/home/<local username here>/.local/bin`  


## Local setup

From root of git (dni-backend directory)

*  Install Docker compose at:  
    `https://docs.docker.com/compose/install/`  
    *   Enter the following in your terminal to add yourself to the docker group:  
        `sudo usermod -aG $USER`  
    *  Restart machine to allow changes to be made (adding you to docker group)  
  
*  Enter the following in your terminal   
    `sudo chmod -R ugo+rwX ./storage ./bootstrap`  

*  Copy the`.example.env` file from dni-backend and rename the copied file `.env file`  
  
*  Enter the following in your terminal at dni-backend directory level:   
    `bookcloth local:initialize`  
    `composer install --ignore-platform-reqs`  
    `docker-compose run --entrypoint npm frontend install`  
    Note:- this will install npm into the container holding the front-end node modules  
    `./dartisan key:generate`   
    `./dartisan migrate`  
    `./dartisan db:seed`  
    `docker-compose up`
  
The site should now be running at `localhost:8000` (if serving on a VM, you may need to swap `localhost` for your VM IP address).
If you find that views are not properly loading, you should check whether the `storage` directory is writeable by the webuser.
For the moment, the simplest approach on a personal VM is `chmod -R ugo+rwX storage` (although upgraded versions will bring in a new approach).

## Elasticsearch for search results and datasets.
Prerequisites:  
   * `you have kubectl installed`
   * `you have gcloud sdk installed`
   * `you are on the dni-dev cluster namespace`  

*  At dni-backend dir run the following command:  
    `docker-compose run phpfpm ip addr`  
    This should return ip addresses.  
    Look for `BROADCAST` heading and take the `inet` address under that.   
    
    * Find the `.env` file of dni-backend and add the following variable, along with the address you just found.   
    `TDATIM_ES_PROXY=http://XXX.XX.XX.X/X.`

    * Now replace the last digit of your ip address with `1`.  
        i.e: `123.456.7/10` would now be `123.456.1`   

    * Then append the port number associated with elasticsearch.  
        i.e:  `TDATIM_ES_PROXY=http://123.456.1:9200`  


    Once the above has been completed:  
    * `run a kubectl port-forward command for elasticsearch in a terminal`  
    * Keep this port running in the terminal. This keeps the connection running.  
    * `docker-compose up`  

    Now when you hit localhost:8000, datasets and search results should show.

## Useful GIT commands
* To get the status of repo you are in:     
    `git status`   
* To get the version of the branch you are on:   
    `git branch -v`  
* To view all the branches of your repo:  
     `git branch -a`   
* To checkout a branch to your local use the following cmd with the branch name:  
    e.g `git checkout branchNameHere`  
* To check what version you have cloned run the following command and compare the number returned with the number in the gitLab repo.  
    `git rev-parse HEAD`  
* To pull down the latest merged changes from a specific branch from the repo:  
    `git pull branchNameHere`  
* To pull the latest changes from your upstream branch:   
    `git pull`  

If this is your first time using git, it might be worth looking at a GIT tutorial for basics.  
Once you are familiar with the basics, it can be useful to find a 'git cheatsheet' that works for you.  
An example would be: `https://about.gitlab.com/images/press/git-cheat-sheet.pdf` 
