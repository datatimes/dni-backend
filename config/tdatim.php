<?php

return [
    'category-server' => [
        'en' => env('TDATIM_CATEGORY_SERVER_EN', 'http://cs-en.example.com'),
        'fr' => env('TDATIM_CATEGORY_SERVER_FR', 'http://cs-fr.example.com'),
    ],
    'lintol-api' => 'http://ltl.ev.openindustry.in',
    'location-openfaas-function' => 'http://of.ev.openindustry.in/function/berlin',
    'categories-yaml' => env('TDATIM_CATEGORIES_YAML', resource_path('data/categories-en.yaml')),
    'categories-yaml-cache-seconds' => 60,
    'graphics-renderer-endpoint' => env('TDATIM_GRAPHICS_RENDERER_ENDPOINT', 'http://172.17.0.1:5000/get_graphic_as_picture/'),
    'force-ssl' => !env('ALLOW_HTTP', false),
    'es-proxy' => env('TDATIM_ES_PROXY', false)
];
