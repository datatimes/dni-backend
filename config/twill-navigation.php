<?php

return [
    'investigationPages' => [
        'title'     => 'InvestigationPages',
        'module'    => true
    ],

    'articles' => [
        'title'     => 'Articles',
        'module'    => true
    ],

    'organizations' => [
        'title'     => 'Organizations',
        'module'    => true
    ],

    'datasets' => [
        'title'     => 'Datasets',
        'module'    => true
    ],

    'topics' => [
        'title'     => 'Topics',
        'module'    => true
    ],

];
