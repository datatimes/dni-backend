<?php

return [
 'file_library' => [
        'disk' => env('FILE_LIBRARY_DISK', 'twill-s3'),
        'endpoint_type' => env('FILE_LIBRARY_ENDPOINT_TYPE', 's3'),
        'cascade_delete' => env('FILE_LIBRARY_CASCADE_DELETE', true),
        'file_service' => env('FILE_LIBRARY_FILE_SERVICE', 'A17\Twill\Services\FileLibrary\Disk'),
        'acl' => env('FILE_LIBRARY_ACL', 'public-read'),
        'prefix_uuid_with_local_path' => false,
        'allowed_extensions' => ['json'],
    ],
 'block_editor' => [
        'files' => ['graphics']
    ],
 'enabled' => [
    'media-library' => false,
    'file-library' => true,
    'activitylog' => false,
    ]
];
