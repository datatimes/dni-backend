<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDatasetsTables extends Migration
{


    public function up()
    {
        Schema::create(
            'datasets',
            function (Blueprint $table) {
                // this will create an id, a "published" column, and soft delete and timestamps columns
                $table->uuid('id');
                $table->primary('id');
                $table->softDeletes();
                $table->timestamps();
                $table->boolean('published')->nullable();

                $table->integer('position')->unsigned()->nullable();

                // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
                // $table->timestamp('publish_start_date')->nullable();
                // $table->timestamp('publish_end_date')->nullable();
            }
        );

        Schema::create(
            'dataset_revisions',
            function (Blueprint $table) {
                $table->increments('id')->unique();
                $table->timestamps();
                $table->json('payload');
                $table->uuid("dataset_id")->unsigned()->index();
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign("dataset_id")->references('id')->on("datasets")->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            }
        );

    }//end up()


    public function down()
    {
        Schema::dropIfExists('dataset_revisions');
        Schema::dropIfExists('datasets');

    }//end down()


}//end class
