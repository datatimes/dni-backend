<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTypesForBlocksTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'blocks',
            function (Blueprint $table) {
                $table->dropColumn('blockable_id');
            }
        );

        Schema::table(
            'blocks',
            function (Blueprint $table) {
                $table->uuid('blockable_id')->nullable();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'blocks',
            function (Blueprint $table) {
                $table->dropColumn('blockable_id');
            }
        );

        Schema::table(
            'blocks',
            function (Blueprint $table) {
                $table->integer('blockable_id')->nullable()->unsigned();
            }
        );

    }//end down()


}//end class
