<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeaturesToArticlesTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'articles',
            function (Blueprint $table) {
                $table->string('title')->nullable();
                $table->string('type')->nullable();
                $table->date('published_at');
                $table->string('outlet');
                $table->string('author');
                $table->string('link', 500);
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'articles',
            function (Blueprint $table) {
                $table->dropColumn('title');
                $table->dropColumn('type');
                // $table->dropColumn('articles_id');
                $table->dropColumn('published_at');
                $table->dropColumn('outlet');
                $table->dropColumn('author');
                $table->dropColumn('link');
            }
        );

    }//end down()


}//end class
