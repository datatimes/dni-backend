<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleInvestigationPageTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'article_investigation_page',
            function (Blueprint $table) {
                $table->uuid('article_id');
                $table->uuid('investigation_page_id');
                $table->primary(['article_id', 'investigation_page_id']);
                $table->foreign('article_id')->references('id')->on('articles');
                $table->foreign('investigation_page_id')->references('id')->on('investigation_pages');
                $table->integer('position')->unsigned();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_investigation_page');

    }//end down()


}//end class
