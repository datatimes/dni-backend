<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeaturesToOrganizationsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'organizations',
            function (Blueprint $table) {
                $table->string('name');
                $table->string('type');
                $table->string('link');
                $table->string('contact');
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'organizations',
            function (Blueprint $table) {
                $table->dropColumn('name');
                $table->dropColumn('type');
                $table->dropColumn('link');
                $table->dropColumn('contact');
            }
        );

    }//end down()


}//end class
