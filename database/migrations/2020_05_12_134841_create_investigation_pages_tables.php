<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestigationPagesTables extends Migration
{


    public function up()
    {
        Schema::create(
            'investigation_pages',
            function (Blueprint $table) {
                // this will create an id, a "published" column, and soft delete and timestamps columns
                $table->uuid('id')->unique();
                $table->primary('id');
                $table->softDeletes();
                $table->timestamps();
                $table->boolean('published')->nullable();

                $table->integer('position')->unsigned()->nullable();
                // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
                // $table->timestamp('publish_start_date')->nullable();
                // $table->timestamp('publish_end_date')->nullable();
            }
        );

        Schema::create(
            'investigation_page_translations',
            function (Blueprint $table) {
                $table->uuid('id')->unique();
                $table->primary('id');
                $table->softDeletes();
                $table->timestamps();
                $table->boolean('published');

                $table->string('locale', 6)->index();
                $table->boolean('active');
                $table->uuid("investigation_page_id");
                $table->foreign("investigation_page_id", "fk_investigation_page_translations_investigation_page_id")->references('id')->on($table)->onDelete('CASCADE');
                $table->unique(["investigation_page_id", 'locale']);
                $table->string('title', 200)->nullable();
                $table->text('description')->nullable();
            }
        );

        /*
            Schema::create('investigation_page_slugs', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->primary('id');
            $table->softDeletes();
            $table->timestamps();
            $table->boolean('published');

            $table->string('slug');
            $table->string('locale', 6)->index();
            $table->boolean('active');
            $table->uuid("investigation_page_id")->unsigned();
            $table->foreign("investigation_page_id", "fk_investigation_page_translations_investigation_page_id")->references('id')->on($table)->onDelete('CASCADE')->onUpdate('NO ACTION');

        });*/

        Schema::create(
            'investigation_page_revisions',
            function (Blueprint $table) {
                $table->uuid('id')->unique();
                $table->primary('id');
                $table->timestamps();
                $table->json('payload');
                $table->uuid("investigation_page_id")->unsigned()->index();
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign("investigation_page_id")->references('id')->on("investigation_pages")->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            }
        );

    }//end up()


    public function down()
    {
        Schema::dropIfExists('investigation_page_revisions');
        Schema::dropIfExists('investigation_page_translations');
        // Schema::dropIfExists('investigation_page_slugs');
        Schema::dropIfExists('investigation__page_revisions');
        Schema::dropIfExists('investigation__page_translations');
        // Schema::dropIfExists('investigation__page_slugs');
        Schema::dropIfExists('investigation_pages');

    }//end down()


}//end class
