<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationInvestigationPageTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'organization_investigation_page',
            function (Blueprint $table) {
                $table->uuid('organization_id');
                $table->uuid('investigation_page_id');
                $table->primary(['organization_id', 'investigation_page_id']);
                $table->foreign('organization_id')->references('id')->on('organizations');
                $table->foreign('investigation_page_id')->references('id')->on('investigation_pages');
                $table->integer('position')->unsigned();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_investigation_page');

    }//end down()


}//end class
