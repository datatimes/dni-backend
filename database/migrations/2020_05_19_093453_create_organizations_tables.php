<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationsTables extends Migration
{


    public function up()
    {
        Schema::create(
            'organizations',
            function (Blueprint $table) {
                // this will create an id, a "published" column, and soft delete and timestamps columns
                $table->uuid('id');
                $table->primary('id');
                $table->softDeletes();
                $table->timestamps();
                $table->boolean('published')->nullable();

                $table->integer('position')->unsigned()->nullable();

                // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
                // $table->timestamp('publish_start_date')->nullable();
                // $table->timestamp('publish_end_date')->nullable();
            }
        );

        /*
            Schema::create('organization_translations', function (Blueprint $table) {
                $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->boolean('published')->default(true);

            $table->string('locale', 6)->index();
            $table->boolean('active')->default(true);
            $table->uuid("organization_id");
            $table->foreign("organization_id", "fk_organization_translations_organization_id")->references('id')->on("organizations")->onDelete('CASCADE');
            $table->unique(["organization_id", 'locale']);
                $table->string('title', 200)->nullable();
                $table->text('description')->nullable();
        });*/

        /*
            Schema::create('organization_slugs', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->primary('id');
            $table->softDeletes();
            $table->timestamps();
            $table->boolean('published');

            $table->string('slug');
            $table->string('locale', 6)->index();
            $table->boolean('active');
            $table->uuid("organization_id")->unsigned();
            $table->foreign("organization_id", "fk_article_translations_article_id")->references('id')->on($table)->onDelete('CASCADE')->onUpdate('NO ACTION');

        });*/

        /*
            Schema::create('organization_revisions', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->timestamps();
            $table->json('payload');
            $table->uuid("organization_id")->unsigned()->index();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign("organization_id")->references('id')->on("organizations")->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

        });*/

    }//end up()


    public function down()
    {
        Schema::dropIfExists('organization_revisions');
        Schema::dropIfExists('organization_translations');
        Schema::dropIfExists('organization_slugs');
        Schema::dropIfExists('organizations');

    }//end down()


}//end class
