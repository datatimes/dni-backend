<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class MakeArticlesPublishedAtNullable extends Migration
{


    public function up()
    {
        Schema::table(
            'organizations',
            function (Blueprint $table) {
                $table->string('type')->nullable()->change();
                $table->string('contact')->nullable()->change();
                $table->string('link', 500)->nullable()->change();
            }
        );
        Schema::table(
            'articles',
            function (Blueprint $table) {
                $table->date('published_at')->nullable()->change();
                $table->string('outlet')->nullable()->change();
                $table->string('author')->nullable()->change();
                $table->string('link', 500)->nullable()->change();
            }
        );

    }//end up()


    public function down()
    {

    }//end down()


}//end class
