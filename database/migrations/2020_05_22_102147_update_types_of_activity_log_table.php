<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTypesOfActivityLogTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'activity_log',
            function (Blueprint $table) {
                $table->dropColumn('subject_id');
            }
        );

        Schema::table(
            'activity_log',
            function (Blueprint $table) {
                $table->uuid('subject_id')->nullable();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'activity_log',
            function (Blueprint $table) {
                $table->dropColumn('subject_id');
            }
        );

        Schema::table(
            'activity_log',
            function (Blueprint $table) {
                $table->integer('subject_id')->nullable();
            }
        );

    }//end down()


}//end class
