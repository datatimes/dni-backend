<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeaturesToInvestigationPagesTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'investigation_pages',
            function (Blueprint $table) {
                $table->string('name', 1000)->nullable();
                $table->json('description')->nullable();
                $table->json('coll_doc_link')->nullable();
                $table->string('foi_feed_link')->nullable();
                // Temporary nullable
                $table->json('vega_graphics')->nullable();
                // Temporary nullable
                $table->json('topics')->nullable();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'investigation_pages',
            function (Blueprint $table) {
                $table->dropColumn('name');
                // $table->dropColumn('investigation_pages_id');
                $table->dropColumn('description');
                $table->dropColumn('coll_doc_link');
                $table->dropColumn('foi_feed_link');
                $table->dropColumn('topics');
                $table->dropColumn('vega_graphics');
            }
        );

    }//end down()


}//end class
