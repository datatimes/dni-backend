<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatasetInvestigationPageTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'dataset_investigation_page',
            function (Blueprint $table) {
                $table->uuid('dataset_id');
                $table->uuid('investigation_page_id');
                $table->primary(['dataset_id', 'investigation_page_id']);
                $table->foreign('dataset_id')->references('id')->on('datasets');
                $table->foreign('investigation_page_id')->references('id')->on('investigation_pages');
                $table->integer('position')->unsigned();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataset_investigation_page');

    }//end down()


}//end class
