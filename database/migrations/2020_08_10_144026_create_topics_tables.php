<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopicsTables extends Migration
{


    public function up()
    {
        Schema::create(
            'topics',
            function (Blueprint $table) {
                $table->uuid('id');
                $table->primary('id');
                $table->string('title', 100);
                $table->softDeletes();
                $table->timestamps();
                $table->boolean('published')->nullable();
                $table->integer('position')->unsigned()->nullable();
            }
        );

        Schema::create(
            'topic_revisions',
            function (Blueprint $table) {
                $table->uuid('id')->unique();
                $table->primary('id');
                $table->timestamps();
                $table->json('payload');
                $table->uuid("topic_id")->index();
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign("topic_id")->references('id')->on("topics")->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            }
        );

    }//end up()


    public function down()
    {
        Schema::dropIfExists('topic_revisions');
        Schema::dropIfExists('topics');

    }//end down()


}//end class
