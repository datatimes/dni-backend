<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationPageTopicTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'investigation_page_topic',
            function (Blueprint $table) {
                $table->uuid('topic_id');
                $table->uuid('investigation_page_id');
                $table->primary(['topic_id', 'investigation_page_id']);
                $table->foreign('topic_id')->references('id')->on('topics');
                $table->foreign('investigation_page_id')->references('id')->on('investigation_pages');
                $table->integer('position')->unsigned();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigation_page_topic');

    }//end down()


}//end class
