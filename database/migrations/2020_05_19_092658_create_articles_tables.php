<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesTables extends Migration
{


    public function up()
    {
        Schema::create(
            'articles',
            function (Blueprint $table) {
                // this will create an id, a "published" column, and soft delete and timestamps columns
                $table->uuid('id');
                $table->primary('id');
                $table->softDeletes();
                $table->timestamps();
                $table->boolean('published')->nullable();

                $table->integer('position')->unsigned()->nullable();

                // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
                // $table->timestamp('publish_start_date')->nullable();
                // $table->timestamp('publish_end_date')->nullable();
            }
        );

        /*
            Schema::create(
            'article_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->softDeletes();
                $table->timestamps();
                $table->boolean('published')->default(true);

                $table->string('locale', 6)->index();
                $table->boolean('active')->default(true);
                $table->uuid("article_id");
                $table->foreign("article_id", "fk_article_translations_article_id")->references('id')->on("articles")->onDelete('CASCADE');
                $table->unique(["article_id", 'locale']);
                $table->string('title', 200)->nullable();
                $table->text('description')->nullable();
            }
        );*/

        /*
            Schema::create('article_slugs', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->primary('id');
            $table->softDeletes();
            $table->timestamps();
            $table->boolean('published');

            $table->string('slug');
            $table->string('locale', 6)->index();
            $table->boolean('active');
            $table->uuid("article_id")->unsigned();
            $table->foreign("article_id", "fk_article_translations_article_id")->references('id')->on($table)->onDelete('CASCADE')->onUpdate('NO ACTION');

        });*/

        /*
            Schema::create(
            'article_revisions', function (Blueprint $table) {
                $table->increments('id')->unique();
                $table->timestamps();
                $table->json('payload');
                $table->uuid("article_id")->unsigned()->index();
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign("article_id")->references('id')->on("articles")->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            }
        );*/

    }//end up()


    public function down()
    {
        // Schema::dropIfExists('article_revisions');
        // Schema::dropIfExists('article_translations');
        // Schema::dropIfExists('article_slugs');
        Schema::dropIfExists('articles');

    }//end down()


}//end class
