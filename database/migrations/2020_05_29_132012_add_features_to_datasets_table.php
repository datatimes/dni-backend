<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeaturesToDatasetsTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'datasets',
            function (Blueprint $table) {
                $table->string('description')->nullable();
                $table->string('link')->nullable();
                $table->uuid('organization_id')->nullable();
                $table->foreign('organization_id')->references('id')->on('organizations');
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'datasets',
            function (Blueprint $table) {
                $table->dropColumn('description');
                $table->dropColumn('link');
                $table->dropColumn('organization_id');
            }
        );

    }//end down()


}//end class
