<?php

use App\Models\InvestigationPage;
use Illuminate\Database\Seeder;

class InvestigationPagesTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('investigation_pages')->delete();
        $json = File::get("scripts/investigationPagesData.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            InvestigationPage::create(
                [
                    'name'          => $obj->title,
                    'description'   => $obj->description,
                // 'relevant_organizations' => $obj->relevantOrganizations,
                    'coll_doc_link' => $obj->collaborativeDocuments,
                    'foi_feed_link' => $obj->foiFeedLink,
                // 'topics' => $obj->topics,
                // 'articles' => $obj->articles,
                // 'datasets' => $obj->datasets,
                // 'vegaGraphics' => $obj->title,
                ]
            );
        }

    }//end run()


}//end class
