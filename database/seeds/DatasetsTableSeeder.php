<?php

use App\Models\Dataset;
use Illuminate\Database\Seeder;

class DatasetsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('datasets')->delete();
        $json = File::get("scripts/investigationPagesData.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            foreach ($obj->datasets as $attribute) {
                Dataset::create(
                    [
                        'description' => $attribute->description,
                    ]
                );
            }
        }

    }//end run()


}//end class
