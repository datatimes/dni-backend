<?php

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->delete();
        $json = File::get("scripts/investigationPagesData.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            foreach ($obj->articles as $attribute) {
                Article::create(
                    [
                        'title'        => $attribute->description,
                        'type'         => $attribute->type,
                        'published_at' => $attribute->date,
                        'outlet'       => $attribute->outlet,
                        'author'       => $attribute->author,
                        'link'         => $attribute->link,
                    ]
                );
            }
        }

    }//end run()


}//end class
