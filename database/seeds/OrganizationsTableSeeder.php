<?php

use App\Models\Organization;
use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->delete();
        $json = File::get("scripts/investigationPagesData.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            foreach ($obj->relevantOrganizations as $attribute) {
                Organization::create(
                    [
                        'name'    => $attribute->description,
                        'type'    => $attribute->type,
                        'link'    => $attribute->link,
                        'contact' => $attribute->contact,
                    ]
                );
            }
        }

    }//end run()


}//end class
