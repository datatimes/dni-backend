<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(InvestigationPagesTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(OrganizationsTableSeeder::class);
        $this->call(DatasetsTableSeeder::class);

    }//end run()


}//end class
